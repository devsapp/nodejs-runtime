const path = require("path");
const fs = require("fs-extra");
const core = require('@serverless-devs/core')
const execa = require("execa");

const currentPath = process.cwd();
class DeployComponent {
  async deploy(inputs) {
    const BestPractice = await core.loadComponent('devsapp/nodejs-best-practice');
    return await BestPractice.deploy(
      inputs,
      (deployPath) => [
        {
          title: "Copy files to publish path",
          task: async () => {
            await fs.copy(
              path.join(currentPath, 'src'),
              path.join(deployPath),
              {
                filter: (src) => {
                  const fileName = src.replace(path.join(currentPath, 'src/'), '');
                  if (fileName === 'package.json') return false;
                  return true
                }
              }
            );
            await fs.copy(
              path.join(__dirname, "template"),
              path.join(deployPath),
              {}
            );
            // 提取 dependencies 的依赖
            const deployJson = fs.readJsonSync(path.join(deployPath, "package.json"));
            const { dependencies = {} } = fs.readJsonSync(path.join(currentPath, "src/package.json"));
            deployJson.dependencies = { ...deployJson.dependencies, ...dependencies };
            fs.writeJsonSync(path.join(deployPath, "package.json"), deployJson, { spaces: 2 });
          },
        },
      ],
      (deployPath) => [
        {
          title: "Update package.json",
          task: async () => {
            await fs.copy(
              path.join(currentPath, "src/package.json"),
              path.join(deployPath, "package.json")
            );
          },
        },
      ]
    );
  }
}

module.exports = DeployComponent;
